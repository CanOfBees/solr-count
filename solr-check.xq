(:~ 
 : pull a mess of XML out of solr
 : and store it in BaseX.
 :)
 
(:
 set intparse true; set addcache true; set ftindex true; set maxlen 200; set maxcats 10000; create db solr-test
:)

declare variable $solr := 'http://porter.lib.utk.edu:8080/solr/collection1/select?q=PID';
declare variable $all-pids := file:read-text-lines('solr-pid-list.txt');

for $entry in $all-pids
let $mod-entry := ":" || fn:replace($entry, ":", "\\:")
let $id-entry := fn:replace($entry, ":", "-")
let $query := $solr || fn:encode-for-uri($mod-entry) || "&amp;wt=xml"

return(
  xquery:fork-join(
    for tumbling window $window in $all-pids
    start at $start when true()
    end at $end when $end - $start eq 3
    return(
    for $w in $window
    let $mod-w := ":" || fn:replace($w, ":", "\\:")
    let $id-w := fn:replace($w, ":", "-")
    let $query := $solr || fn:encode-for-uri($mod-w) || "&amp;wt=xml"
    let $fx := fetch:xml($query)/response/result/doc
    return function() { file:write("retest" || $id-w || ".xml", $fx, map { "method": "xml", "omit-declaration-no": "no" } ) }
    )
  )
)
    
  (: for $solr-doc in fetch:xml($query)/response/result/doc
  let $path := "data/"
  return(
    file:write($path || $id-entry || '.xml', $solr-doc, map { 'method': 'xml', 'omit-xml-declaration': 'no'}) :)
    (: db:add("solr-test", $solr-doc, $id-entry, map { 'addcache': true()  }),
    db:optimize("solr-test", true(), map { 'textindex': true(), 'attrindex': true(), 'tokenindex': true(), 'ftindex': true()}) :)
